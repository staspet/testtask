<?php

namespace Core;

final class App
{
    public $currentUrl = [];

    public function run()
    {
        //Обработвем URL
        if ($_SERVER['QUERY_STRING']) $getString = '?'.$_SERVER['QUERY_STRING'];
        $formatUrl = str_replace($getString,'',$_SERVER['REQUEST_URI']);
        $this->currentUrl = explode('/', $formatUrl);

        //Если пусто перенаправим на index/index
        if (!$this->currentUrl[1])  $this->currentUrl[1] = 'index';
        if (!$this->currentUrl[2])  $this->currentUrl[2] = 'index';

        $controllerClass = 'Core\Controllers\\' . ucfirst($this->currentUrl[1]) . 'Controller';

        //Проверим существщвание контроллера
        if (!file_exists($controllerClass . '.php')) {
            View::showError(404,'Контноллер не найден'); die;
        }

        //Создадим экземпляр контроллера
        $controller = new $controllerClass;

        //Проверим есть ли экшен
        $viewAction = 'action' . ucfirst($this->currentUrl[2]);
        if (!method_exists($controller, $viewAction)) {
            View::showError(404,'Экшен не найден');  die;
        }

        //Вызываем экшен
        $controller->$viewAction();

    }

}