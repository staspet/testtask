<?php
namespace Core\Controllers;

use Core\Controller;
use Core\View;

class IndexController extends Controller
{
    public function actionIndex()
    {
        View::render('index');
    }
}