<?php
namespace Core\Controllers;

use Core\Controller;
use Core\Db;

//Класс для миграции данных с сервера
class MigrationController extends Controller
{

    public function actionUser()
    {
        $data = file_get_contents('http://jsonplaceholder.typicode.com/users');
        $jsonData = json_decode($data, true);
        foreach ($jsonData as $jsonUser) {
            $sql = "INSERT INTO users (id, first_name, last_name, phone, email) VALUES (?,?,?,?,?)";
            Db::getConn()->prepare($sql)->execute([$jsonUser['id'], $jsonUser['name'], $jsonUser['username'], $jsonUser['phone'], $jsonUser['email'],]);
        }
        echo "Done";
    }

    public function actionPosts()
    {
        $data = file_get_contents('http://jsonplaceholder.typicode.com/posts');
        $jsonData = json_decode($data, true);
        foreach ($jsonData as $jsonUser) {
            $sql = "INSERT INTO posts (id, user_id, title, body ) VALUES (?,?,?,?)";
            Db::getConn()->prepare($sql)->execute([$jsonUser['id'], $jsonUser['userId'], $jsonUser['title'], $jsonUser['body']]);
        }
        echo "Done";
    }

}