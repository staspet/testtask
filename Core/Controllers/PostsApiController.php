<?php
namespace Core\Controllers;

use Core\ActiveRecords\Posts;

class PostsApiController
{
    public function actionGetMore()
    {
        parse_str($_SERVER['QUERY_STRING'], $get_params);
        echo $this->getMore ('1', $get_params['lastid']);
    }

    //Получить еще 5 записей пользователя
    public function getMore($idUser, $lastId)
    {
        $response = '';

        $posts = Posts::find('where id < ' . $lastId . ' and user_id = ' . $idUser . '  ORDER BY `id` desc limit 5 ');
        foreach ($posts as $post) {
            $response .= \Core\View::renderPartial('postItem', [
                'post' => $post,
            ]);
        }

        echo $response;
    }
}