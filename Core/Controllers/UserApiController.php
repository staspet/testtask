<?php


namespace Core\Controllers;


use Core\ActiveRecords\Posts;
use Core\ActiveRecords\Users;
use Core\View;

class UserApiController
{
    //Фильтр позователей по имени
    public function actionFindUsers()
    {
        parse_str($_SERVER['QUERY_STRING'], $get_params);
        $response = [];
        if ($get_params['name'] ) {
            $users = Users::find('where first_name like "%' . $get_params['name'] . '%" or last_name like "%' . $get_params['name'] . '%" ');
            foreach ($users as $user) {
                $response['users'][] = View::renderPartial('tbodyUsers',[
                    'user' => $user,
                ]);
            }
        } elseif ($get_params['name'] == '') {
            $users = Users::find();
            foreach ($users as $user) {
                $response['users'][] = View::renderPartial('tbodyUsers',[
                    'user' => $user,
                ]);
            }
        }

        if (count($users) == 1) {
            $posts = Posts::find('where user_id = ' . $users[0]->id . ' ORDER BY `id` desc limit 5 ');
            if ($posts) {
                $response['posts'][] = View::renderPartial('tbodyPosts',[
                    'posts' => $posts,
                    'user' => $users[0],
                ]);
            }

        }

        echo json_encode($response);
    }

}