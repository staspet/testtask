<?php
namespace Core;

use PDO;
use PDOException;

class Db
{
    const HOST = 'localhost';
    const DBNAME = 'StasDB';
    const USERNAME = 'StasShehida';
    const PASSWORD = 'secret';

    private static $_instance = null;

    private function __construct ()
    {
        try {
            $this->_instance = new PDO('mysql:host='.self::HOST.';dbname='.self::DBNAME, self::USERNAME, self::PASSWORD);
        } catch (PDOException $e) {
            View::showError(404,'Подключение к базе данных не удалось: ' . $e->getMessage()); die;
        }
    }

    private function __clone () {}
    private function __wakeup () {}

    public static function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public static function getConn() {
        return self::getInstance()->_instance;
    }





}