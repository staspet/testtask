<?php
namespace Core;

use yii\db\ActiveQuery;

abstract class Model
{
    public $rows = []; //Тут будем хранить поля таблицы
    public static function tableName(){}

    //Получим массив экземпляров модели
    public static function find($condition = '')
    {
        $classPath = get_called_class();

        $query = 'SELECT * FROM ' . $classPath::tableName() . ' ' .  $condition;
        $stmt = Db::getConn()->query($query);

        $models = [];

        while ($row = $stmt->fetch())
        {
            //Создадим экземпляр модели
            $model = new $classPath;

            //Добавим свойства в класс по шаблону таблицы
            foreach ($row as $k => $r) $model->$k = $r;
            $models[] = $model;
        }

        return $models;
    }


}