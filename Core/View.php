<?php
namespace Core;

class View
{
    //Рендер вида с лейаутом
    public static function render($viewName, $params = [])
    {
        extract($params);
        $content = self::renderPartial($viewName, $params);
        $view = self::renderPartial('layout', [
            'content' => $content,
        ]);

        echo $view;
    }

    //Рендер отделного блока
    public static function renderPartial($viewName, $params = [])
    {
        if (file_exists('public\views\\' . $viewName . '.php')) {
            extract($params);
            ob_start();
            include 'public\views\\' . $viewName . '.php';
            $content = ob_get_contents();
            ob_end_clean();
            return $content;
        } else {
            self::showError(500,'Файл вида не найден');
        }
    }

    //Страница ошибок
    public static function showError($status, $message)
    {
        self::render('error', [
            'message' => $message,
            'status' => $status,
        ]);
    }
}