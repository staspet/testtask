<?php
use Core\App;

//Загрузка классов
spl_autoload_register(function($class){
    $patch = str_replace('\\', '/', $class . '.php');
    if (file_exists($patch)) {
        require $patch;
    }
});

//Запускаем приложение
(new App())->run();
