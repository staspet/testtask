<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <input id="filter-input" class="form-control" type="text" placeholder="Search by name">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">first_name</th>
                    <th scope="col">last_name</th>
                    <th scope="col">phone</th>
                    <th scope="col">email</th>
                </tr>
                </thead>
                <tbody id="tbody-users">

                </tbody>
            </table>
            <div id="posts">

            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {

        //Отслеживаем поле фильтра
        $('#filter-input').on("keyup", function() {
            updateUsers( $( this ).val())
        });

        //Функция обновления таблицы пользователей
        function updateUsers(name) {
            $('#posts').html('');
            $.ajax({
                url: "/userapi/findusers?name=" +  name,
            }).done(function(data) {
                data = JSON.parse(data);
                if (!data.users) data.users = 'Пользователи не найдены';
                $('#tbody-users').html( data.users );
                if (data.posts) $('#posts').html( data.posts );
            });
        }

        //Загрузим пользователей
        updateUsers($('#filter-input').val());
    });

</script>
<br>
<br>