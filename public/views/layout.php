<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Test task - Stas Shehida</title>
</head>
<body>
<section style="background-color: #353b42; color:white; font-size: 120%; padding: 20px 0 20px;">
    <div class="container">
        <div class=" d-flex justify-content-between">
            <div class=""> Тестовое задание </div>
            <div class="">
                <div>Стас Шегида / (063) 815 84 81</div>
            </div>
        </div>
    </div>
</section>
<section>
    <?= $content?>
</section>
<section style="background-color: #353b42;  color:white; font-size: 120%;  padding: 20px 0 20px;">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                25 Января 2021
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>
