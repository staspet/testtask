<?php
?>
<div class="col-md-4" >
    <div class="card card-posts" data-id="<?=$post->id?>" >
        <div class="card-body">
            <h5 class="card-title" style="font-weight: 600; font-size: 120%;">#<?=$post->id . ' ' . $post->title?></h5>
            <p class="card-text">
                <?= mb_strimwidth($post->body, 0, 80, "...") ?>
            </p>
        </div>
    </div>
</div>
