<br>
<div class="card">
    <div class="card-header">
        <h3><?=$user->first_name?> posts:</h3>
    </div>
    <div class="card-body" >
        <div id="post-container" class="row ">
            <?php foreach($posts as  $post):?>
                <?= \Core\View::renderPartial('postItem', [
                    'post' => $post,
                ])?>
            <?php endforeach;?>
        </div>
        <button id="load-more" type="button" class="btn btn-primary">Load yet 5 posts...</button>
    </div>
</div>
<script>
    $("#load-more").on( "click", function() {
        //Получим последний Post на странице
        var lastIdpost = $( '.card-posts' ).last().attr('data-id');
        $.ajax({
            url: "/postsapi/getmore?lastid=" +  lastIdpost,

        }).done(function(data) {
            $('#post-container').append(data);
            if (!data) {
                $('#load-more').remove();
            }
        });
    });
</script>
<style>
    .card-posts{margin-bottom: 15px}
</style>